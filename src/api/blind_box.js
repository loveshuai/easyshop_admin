import request from '@/utils/request';

export function blind_box_list(params) {
    return request({
        url: '/blind_box',
        method: 'get',
        params: params,
    });
}

export function blind_box_post(params) {
    return request({
        url: '/blind_box',
        method: 'post',
        data: params,
    });
}

export function blind_box_get(id, params) {
    return request({
        url: '/blind_box/' + id,
        method: 'get',
        params: params,
    });
}

export function blind_box_del(id, params = '') {
    return request({
        url: '/blind_box/' + id,
        method: 'delete',
        params: params,
    });
}

export function blind_box_put(params) {
    return request({
        url: '/blind_box',
        method: 'put',
        params: params,
    });
}